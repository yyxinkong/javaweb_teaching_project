--用户权限管理，共需要五张表，users表已经有了，此处只有四张表

create table permission 
(
   id                 number(10)           not null,
   ermission         varchar2(50),
   url                varchar2(200),
   remark              varchar2(100),
   constraint PK_PERMISSION primary key (id)
);
create sequence permission_id minvalue 0 start with 0 increment by 1;

create table permissionRole 
(
   id                 number(10)           not null,
   permissionid       number(10),
   roleid             number(10),
   constraint PK_PERMISSIONROLE primary key (id)
);
create sequence permissionRole_id minvalue 0 start with 0 increment by 1;

create table role
(
   id                 number(10)    not null,
   rolename           varchar(50),
   remark               varchar(100),
   constraint PK_ROLE primary key (id)
);
create sequence role_id minvalue 0 start with 0 increment by 1;

create table userRole 
(
   id                 number(10)           not null,
   userid            number(10)         references users(id),
   roleid            number(10)        references role(id),
   constraint PK_USERROLE primary key (id)
);
create sequence userRole_id minvalue 0 start with 0 increment by 1;
