--
create table onlines(
	id number(10) primary key,
	userName varchar2(50) not null,
	ip varchar2(20) not null
	status varchar2(20) default 'login' not null,
	time date not null,		--yyyy-mm-dd hh:mi:ss
);
