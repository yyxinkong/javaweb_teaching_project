基本概念

工作区，带有.git隐藏目录的目录，称为工作区，工作区是编写出来的代码存放的区域。

暂存区，.git目录中的一个文件，叫index文件。Add to Index和git add都是把工作区中的代码存放到暂区。暂存区的代码会被跟踪，但不会进行版本控制的范围。	

本地版本库（本地仓库），.git目录本身就是本地版本库，commit和git commit指的就是把暂存区的数据放入本地版本库，也就进了版本控制。

远程版本库（远程仓库），是github或码云这样的支持git应用的网站，push和git push是把本地版本库中的代码推到远程版本库。

命令

git init		--在当前目录中创建.git的隐藏目录，也就是创建工作区、暂存区和本地版本库

	

git add 。	--把当前文件存入暂存区，git开跟踪这些文件。对应Eclipse的git插件中的Add to index

git commit -m "message"		--把暂存区数据存入本地版本库。对应Eclipse的git插件中的commit

git push		--把本地版本库中的数据存入远程版本库。对应Eclipse的git插件中的push

git clone		--把远程版本库中的代码，克隆到本地，同创建工作区、暂存区、本地版本库。对应Eclipse的git插件中的import->projects from Git
