package edu.yuhf.service;

import java.util.List;

import org.junit.Test;

import edu.yuhf.domain.NativePlace;
import edu.yuhf.service.iface.NativePlaceService;

public class NativePlaceServiceImplTest {

	NativePlaceService nps=new NativePlaceServiceImpl();
	
	@Test
	public void setProvinceSelectedTest() {
		List<NativePlace> list=nps.setProvinceSelected("3701");
		list.forEach((item)->System.out.println(item.getName()+","+item.isSelected()));
	}
	
	@Test
	public void getLevle2() {
		nps.getCity("37").forEach((item)->{System.out.println(item.getName());});
	}
	
	@Test
	public void setCitySelectedTest() {
		List<NativePlace> list=nps.setCitySelected("3701");
		list.forEach((item)->System.out.println(item.getName()+","+item.isSelected()));
	}
}
