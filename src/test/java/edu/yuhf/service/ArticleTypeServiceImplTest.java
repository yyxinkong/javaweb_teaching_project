package edu.yuhf.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import edu.yuhf.domain.ArticleType;
import edu.yuhf.service.iface.ArticleTypeService;

public class ArticleTypeServiceImplTest {

	private static final Logger log=Logger.getLogger(ArticleTypeServiceImplTest.class);
	
	private ArticleTypeService articleTypeService=new ArticleTypeServiceImpl();
	
	@Test
	public void getAllTypeTest() {
		List<ArticleType> list=articleTypeService.getAllType();
		log.debug(list.size());
		list.stream().forEach((node)->{
			log.debug("parent Node:"+node.getName()+","+node.getId());
			node.getSubArticleTypes().forEach((subNode)->{
				log.debug("sub Node:"+subNode.getName()+","+subNode.getId());
			});
		});
	}
}
