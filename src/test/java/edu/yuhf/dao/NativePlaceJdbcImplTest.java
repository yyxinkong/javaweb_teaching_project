package edu.yuhf.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import edu.yuhf.dao.iface.NativePlaceDao;
import edu.yuhf.domain.NativePlace;

public class NativePlaceJdbcImplTest {

	private NativePlaceDao nativePlaceDao=new NativePlaceJdbcDaoImpl();
	
	private static Logger log=Logger.getLogger(NativePlaceJdbcImplTest.class);
	
	@Test
	public void getLevel1Test() {
		List<NativePlace> list=nativePlaceDao.getLevel1();
		list.forEach((item)->{log.debug(item.getName());});
	}
	@Test
	public void getLevel2Test() {
		List<NativePlace> list=nativePlaceDao.getLevel2("37");
		list.forEach((item)->{log.debug(item.getName());});
	}
}
