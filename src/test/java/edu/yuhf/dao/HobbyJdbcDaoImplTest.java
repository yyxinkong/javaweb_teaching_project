package edu.yuhf.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import edu.yuhf.dao.iface.HobbyDao;
import edu.yuhf.dao.iface.UserDao;
import edu.yuhf.domain.Hobby;

public class HobbyJdbcDaoImplTest {

	private Logger log=Logger.getLogger(UserJdbcDaoImplTest.class);
	
	private HobbyDao hobbyDao=new HobbyJdbcDaoImpl();
	private UserDao userDao=new UserJdbcDaoImpl();
	
	@Test
	public void getHobbyTest() {
		List<Hobby> list=hobbyDao.getHobbies();
		list.stream().forEach((item)->log.debug(item.getName()));
	}
	
}
