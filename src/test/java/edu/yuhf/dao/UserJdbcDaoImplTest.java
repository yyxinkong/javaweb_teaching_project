package edu.yuhf.dao;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;

import edu.yuhf.dao.iface.UserDao;
import edu.yuhf.domain.Page;
import edu.yuhf.domain.UMQueryKeyword;
import edu.yuhf.domain.User;
import edu.yuhf.domain.UserDetail;

public class UserJdbcDaoImplTest {

	private Logger log=Logger.getLogger(UserJdbcDaoImplTest.class);
	
	private UserDao userDao=new UserJdbcDaoImpl();
	
	@Test
	public void checkUserTest() {
		int rowNum=userDao.checkUser("admin", "admin1");
		log.debug("checkUser() return:"+rowNum);
	}
	
	@Test
	public void insertUserTest() {
		User user=new User(0,"test43210","test43210","1","test43210@qq.com");
		int key=userDao.insertUser(user);
		log.debug("key is:"+key);
	}
	
	@Test
	public void insertUserDetailTest() {
		UserDetail ud=new UserDetail(0,"3709","3",30);
		int rowNum=userDao.insertUserDetail(ud);
		log.debug(rowNum);
	}
	@Test
	public void queryAllTest() {
		List<Map<String,Object>> list=userDao.queryAll();
		list.stream().forEach((map)->{
			map.keySet().forEach((item)->{
				log.debug(item+":"+map.get(item)+" ");
			});
		});
	}
	@Test
	public void queryUserByIdTest() {
		Map<String,Object> map=userDao.queryUserById("38");
		map.keySet().forEach((item)->{
			log.debug(item+":"+map.get(item)+" ");
		});

	}
	@Test
	public void queryByPageTest() {
		Page<List<Map<String,Object>>> page=new Page<>(2);
		userDao.queryByPage(page);
		page.getPageData().forEach((item)->{
			item.forEach((key,value)->{
				System.out.print(value+",");
			});
			System.out.println();
		});
		
	}
	
	@Test
	public void queryTotalRowNumber() {
		Page<List<Map<String,Object>>> page=new Page<>();
		userDao.getTotalRow(page);
		System.out.println(page.getTotalRow());
	}
	@Test
	public void queryByPageTest1() {
		Page<List<Map<String,Object>>> page=new Page<>(1);
		UMQueryKeyword qk=new UMQueryKeyword("a","","");
		userDao.queryByPage(page,qk);
		log.debug("list size:"+page.getPageData().size());
		page.getPageData().forEach((item)->{
			item.forEach((key,value)->{
				log.debug(value+",");
			});
		});
	}
	
	@Test
	public void getTotalRowTest() {
		Page<List<Map<String,Object>>> page=new Page<>(1);
		UMQueryKeyword qk=new UMQueryKeyword("a","","");
		userDao.getTotalRow(page,qk);
		System.out.println(":::"+page.getTotalRow());
		
	}
}
