package edu.yuhf.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.yuhf.db.utils.JdbcTemplate;

import edu.yuhf.dao.iface.ArticleTypeDao;
import edu.yuhf.domain.ArticleType;

public class ArticleTypeJdbcDaoImpl implements ArticleTypeDao{

	private static final Logger log=Logger.getLogger(ArticleTypeJdbcDaoImpl.class);
	
	@Override
	public List<ArticleType> getParentType() {
		String sql="select * from articleType where parentId=0";
		List<ArticleType> list=JdbcTemplate.query(sql, (rs)->{
			List<ArticleType> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					ArticleType at=new ArticleType(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getString(5));
					list0.add(at);
				}
			} catch (SQLException e) {
				log.debug("getParentType method error,message is "+e.getMessage());
			}
			return list0;
		}, new Object[] {});
		return list;
	}

	@Override
	public List<ArticleType> getSubType() {
		String sql="select * from articleType where parentId!=0";
		List<ArticleType> list=JdbcTemplate.query(sql, (rs)->{
			List<ArticleType> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					ArticleType at=new ArticleType(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getString(5));
					list0.add(at);
				}
			} catch (SQLException e) {
				log.debug("getParentType method error,message is "+e.getMessage());
			}
			return list0;
		}, new Object[] {});
		return list;
	}

}
