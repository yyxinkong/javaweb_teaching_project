package edu.yuhf.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.yuhf.db.utils.JdbcTemplate;

import edu.yuhf.dao.iface.HobbyDao;
import edu.yuhf.domain.Hobby;

public class HobbyJdbcDaoImpl implements HobbyDao{

	public List<Hobby> getHobbies() {
		String sql="select id,name,code from hobby";
		List<Hobby> list=JdbcTemplate.query(sql, (rs)->{
			List<Hobby> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					Hobby hobby=new Hobby(rs.getInt(1),rs.getString(2),rs.getString(3),false);
					list0.add(hobby);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return list0;
		}, null);
		return list;
	}

}
