package edu.yuhf.dao.iface;

import java.util.List;
import java.util.Map;

import edu.yuhf.domain.Hobby;

public interface HobbyDao {
	public List<Hobby> getHobbies();
}
