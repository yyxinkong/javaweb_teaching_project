package edu.yuhf.dao.iface;

import java.util.List;
import java.util.Map;

import edu.yuhf.domain.Article;

public interface ArticleDao {

	public List<Article> getArticleByType(int typeId);
	public List<Map<String,Object>> getArticleMapByType(int typeId);
}
