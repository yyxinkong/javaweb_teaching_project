package edu.yuhf.dao.iface;

import java.util.List;
import java.util.Map;

import edu.yuhf.domain.Page;
import edu.yuhf.domain.UMQueryKeyword;
import edu.yuhf.domain.User;
import edu.yuhf.domain.UserDetail;

public interface UserDao {

	public int checkUser(String name,String password);
	public int insertUser(User user);
	public int insertUserDetail(UserDetail ud);
	public List<Map<String,Object>> queryAll();
	public Map<String,Object> queryUserById(String id);
	public int deleteUsers(String ids);
	public int deleteUserDetail(String ids);
	public int updateUser(User user);
	public int updateUserDetail(UserDetail user);
	
	public void queryByPage(Page<List<Map<String,Object>>> page);
	public void getTotalRow(Page<List<Map<String,Object>>> page);
	
	public void queryByPage(Page<List<Map<String,Object>>> page ,UMQueryKeyword qk);
	public void getTotalRow(Page<List<Map<String,Object>>> page ,UMQueryKeyword qk);
}
