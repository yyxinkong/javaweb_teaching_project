package edu.yuhf.dao.iface;

import java.util.List;

import edu.yuhf.domain.ArticleType;

public interface ArticleTypeDao {

	public List<ArticleType> getParentType();
	public List<ArticleType> getSubType();
}
