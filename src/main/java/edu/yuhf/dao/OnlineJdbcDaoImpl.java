package edu.yuhf.dao;

import java.time.format.DateTimeFormatter;

import com.yuhf.db.utils.JdbcTemplate;

import edu.yuhf.dao.iface.OnlineDao;
import edu.yuhf.domain.Online;

public class OnlineJdbcDaoImpl implements OnlineDao{

	@Override
	public int insertOnline(Online ol) {
		int rowNum=0;
		String sql="insert into onlines values(online_id.nextval,?,?,'login',to_date(?,'yyyy-mm-dd hh24:mi:ss'))";
		DateTimeFormatter dtf=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		rowNum=JdbcTemplate.update(sql, new Object[] {ol.getName(),ol.getIp(),dtf.format(ol.getTime())});
		return rowNum;
	}

}
