package edu.yuhf.service.iface;

import java.util.List;
import java.util.Map;

import edu.yuhf.domain.Hobby;

public interface HobbyService {
	public List<Hobby> getHobbies();
	public List<Hobby> getHobbiesSelected(String selected);
	public void setHobbyName(List<Map<String, Object>> list);
}
