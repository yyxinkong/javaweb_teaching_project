package edu.yuhf.service.iface;

import java.util.List;

import edu.yuhf.domain.NativePlace;

public interface NativePlaceService {

	public List<NativePlace> getProvince();
	public List<NativePlace> setProvinceSelected(String code);
	public List<NativePlace> getCity(String code);
	public List<NativePlace> setCitySelected(String code);
}
