package edu.yuhf.service.iface;

import java.util.List;
import java.util.Map;

import edu.yuhf.domain.Article;

public interface ArticleService {

	public List<Article> getArticle(String typeId);
	public List<Map<String,Object>> getArticleMap(String typeId);
}
