package edu.yuhf.service.iface;

import java.util.List;

import edu.yuhf.domain.ArticleType;

public interface ArticleTypeService {

	public List<ArticleType> getParentType();
	public List<ArticleType> getSubType();
	
	public List<ArticleType> getAllType();
}
