package edu.yuhf.service.iface;

import java.util.List;
import java.util.Map;

import edu.yuhf.domain.Page;
import edu.yuhf.domain.UMQueryKeyword;
import edu.yuhf.domain.User;
import edu.yuhf.domain.UserDetail;

public interface UserService {

	public boolean login(String name,String password);
	public boolean register(User user,UserDetail ud);
	public List<Map<String,Object>> showAll();
	public Map<String,Object> showUserById(String id);
	public boolean removeUsers(String ids);
	public boolean updateUser(User user,UserDetail ud);
	
	public void showByPage(Page<List<Map<String,Object>>> page);
	public void getTotalRow(Page<List<Map<String,Object>>> page);
	
	public void showByPage(Page<List<Map<String,Object>>> page,UMQueryKeyword qk);
	public void getTotalRow(Page<List<Map<String,Object>>> page,UMQueryKeyword qk);
}
