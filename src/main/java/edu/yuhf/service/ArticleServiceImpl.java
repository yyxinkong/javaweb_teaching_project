package edu.yuhf.service;

import java.util.List;
import java.util.Map;

import edu.yuhf.dao.ArticleJdbcDaoImpl;
import edu.yuhf.dao.iface.ArticleDao;
import edu.yuhf.domain.Article;
import edu.yuhf.service.iface.ArticleService;

public class ArticleServiceImpl implements ArticleService{
	
	private ArticleDao articleDao=new ArticleJdbcDaoImpl();
	@Override
	public List<Article> getArticle(String typeId) {
		return articleDao.getArticleByType(Integer.valueOf(typeId));
	}
	@Override
	public List<Map<String, Object>> getArticleMap(String typeId) {
		List<Map<String, Object>> list=articleDao.getArticleMapByType(Integer.valueOf(typeId));
		return list;
	}

}
