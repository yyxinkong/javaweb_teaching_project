package edu.yuhf.service;

import java.util.List;
import java.util.Objects;

import edu.yuhf.dao.NativePlaceJdbcDaoImpl;
import edu.yuhf.dao.iface.NativePlaceDao;
import edu.yuhf.domain.NativePlace;
import edu.yuhf.service.iface.NativePlaceService;

public class NativePlaceServiceImpl implements NativePlaceService{

	private NativePlaceDao nativePlaceDao=new NativePlaceJdbcDaoImpl();
	
	@Override
	public List<NativePlace> getProvince() {
		return nativePlaceDao.getLevel1();
	}

	@Override
	public List<NativePlace> getCity(String code) {
		return nativePlaceDao.getLevel2(code);
	}

	@Override
	public List<NativePlace> setProvinceSelected(String code) {
		List<NativePlace> list=nativePlaceDao.getLevel1();
		String provinceCode=code.substring(0,2);
		list.stream().filter((item)->Objects.equals(item.getCode(),provinceCode)).forEach((item)->item.setSelected(true));
		return list;
	}

	@Override
	public List<NativePlace> setCitySelected(String code) {
		List<NativePlace> list=nativePlaceDao.getLevel2(code.substring(0, 2));
		list.stream().filter((item)->Objects.equals(item.getCode(),code)).forEach((item)->item.setSelected(true));
		return list;
	}
}
