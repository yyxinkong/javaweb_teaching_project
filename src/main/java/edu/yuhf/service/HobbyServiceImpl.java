package edu.yuhf.service;

import java.util.List;
import java.util.Map;

import edu.yuhf.dao.HobbyJdbcDaoImpl;
import edu.yuhf.dao.iface.HobbyDao;
import edu.yuhf.domain.Hobby;
import edu.yuhf.service.iface.HobbyService;

public class HobbyServiceImpl implements HobbyService{

	private HobbyDao hobbyDao=new HobbyJdbcDaoImpl();
	
	public List<Hobby> getHobbies(){
		return hobbyDao.getHobbies();
		
	}

	//把list中的爱好编码转换成中文表示
	public void setHobbyName(List<Map<String, Object>> list) {
		List<Hobby> hobbies=hobbyDao.getHobbies();
		list.forEach((map)->{
			String temp=(String) map.get("HOBBY_CODE");
			for(Hobby hobby:hobbies) {
				if(null==temp) {
					temp="这个人很懒，没有爱好...";
				}else if(temp.indexOf(hobby.getCode())!=-1){
					temp=temp.replace(hobby.getCode(), hobby.getName());
				}
			}
			map.put("HOBBY_CODE", temp);
		});
	}

	@Override
	public List<Hobby> getHobbiesSelected(String selected) {
		List<Hobby> list=hobbyDao.getHobbies();
		list.stream().filter((item)->selected.indexOf(item.getCode())!=-1)
			.forEach((item)->item.setSelected(true));
		
		return list;
	}
}
