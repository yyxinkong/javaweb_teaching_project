package edu.yuhf.service;

import java.util.List;

import org.apache.log4j.Logger;

import edu.yuhf.dao.ArticleTypeJdbcDaoImpl;
import edu.yuhf.dao.iface.ArticleTypeDao;
import edu.yuhf.domain.ArticleType;
import edu.yuhf.service.iface.ArticleTypeService;

public class ArticleTypeServiceImpl implements ArticleTypeService{

	private static final Logger log=Logger.getLogger(ArticleTypeServiceImpl.class);
	
	private ArticleTypeDao articleTypeDao=new ArticleTypeJdbcDaoImpl();
	
	@Override
	public List<ArticleType> getParentType() {
		return articleTypeDao.getParentType();
	}

	@Override
	public List<ArticleType> getSubType() {
		return articleTypeDao.getSubType();
	}

	@Override
	public List<ArticleType> getAllType() {
		List<ArticleType> parentTypes=articleTypeDao.getParentType();
		List<ArticleType> subTypes=articleTypeDao.getSubType();
		parentTypes.forEach((parentNode)->{
			setSubNode(parentNode,subTypes);
		});
		return parentTypes;
	}
	
	private void setSubNode(ArticleType parentNode,List<ArticleType> subTypes) {
		subTypes.stream().filter((subNode)->subNode.getParentId()==parentNode.getId()).forEach((subNode)->{
			parentNode.getSubArticleTypes().add(subNode);
		});

	}

}
