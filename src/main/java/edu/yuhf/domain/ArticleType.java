package edu.yuhf.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleType {

	private int id;
	private String name;
	private int parentId;
	private String url;
	private String remark;
	private List<ArticleType> subArticleTypes=new ArrayList<>();
	public ArticleType(int id, String name, int parentId, String url, String remark) {
		super();
		this.id = id;
		this.name = name;
		this.parentId = parentId;
		this.url = url;
		this.remark = remark;
	}
}
