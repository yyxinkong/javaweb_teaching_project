package edu.yuhf.domain;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Article {

	private int id;
	private String title;
	private String content;
	private LocalDate publistTime;
	private int click;
	private int like;
	private int typeId;
	private int userId;
	private int top;
	private int support;

}
