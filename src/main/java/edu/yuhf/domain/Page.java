package edu.yuhf.domain;

import edu.yuhf.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page<T> {

	private int RowNumber=Constants.ROW_NUMBER;
	private int totalRow;
	private int totalPage;
	private int currentPage;
	
	private String keyword;
	
	private T pageData;

	public Page(int currentPage) {
		super();
		this.currentPage = currentPage;
	}
	

	

}
