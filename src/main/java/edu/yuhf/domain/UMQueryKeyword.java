package edu.yuhf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UMQueryKeyword {
	
	private String name;
	private String sex;
	private String hobbyCode;
	
}
