package edu.yuhf.domain;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Online {

	private int id;
	private String name;
	private String ip;
	private String status;
	private LocalDateTime time;
	
}
