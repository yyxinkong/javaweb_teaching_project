package edu.yuhf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NativePlace {

	private int id;
	private String name;
	private String code;
	private boolean selected;
	

}
