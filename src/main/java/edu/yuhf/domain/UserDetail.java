package edu.yuhf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetail {

	private int id;
	private String nativePlace_code;
	private String hobby_code;
	private int UserId;
	
}
