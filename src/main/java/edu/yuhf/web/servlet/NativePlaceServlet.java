package edu.yuhf.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;

import edu.yuhf.domain.NativePlace;
import edu.yuhf.service.NativePlaceServiceImpl;
import edu.yuhf.service.iface.NativePlaceService;


@WebServlet("/nativePlace.servlet")
public class NativePlaceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private NativePlaceService nativePlaceService=new NativePlaceServiceImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String provinceCode=request.getParameter("code");
		List<NativePlace> list=nativePlaceService.getCity(provinceCode);
		Map<String,Object> map=new HashMap<>();
		if(null==list||list.size()==0) {
			map.put("msg","no data");
		}else {
			map.put("msg","success");
			map.put("result", list);
		}
		String json=JSONObject.toJSONString(map);
		PrintWriter out=response.getWriter();
		out.print(json);
		out.flush();
		out.close();
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
