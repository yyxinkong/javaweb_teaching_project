package edu.yuhf.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.yuhf.service.ArticleServiceImpl;
import edu.yuhf.service.ArticleTypeServiceImpl;
import edu.yuhf.service.iface.ArticleService;
import edu.yuhf.service.iface.ArticleTypeService;


@WebServlet("/articleType.servlet")
public class ArticleTypeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ArticleService articleService=new ArticleServiceImpl();
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String typeId=request.getParameter("typeId");
		request.setAttribute("articles", articleService.getArticleMap(typeId));
		request.getRequestDispatcher("views/articleType.jsp").forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
