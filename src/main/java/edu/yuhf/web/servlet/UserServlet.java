package edu.yuhf.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import edu.yuhf.domain.Hobby;
import edu.yuhf.domain.NativePlace;
import edu.yuhf.domain.Page;
import edu.yuhf.domain.UMQueryKeyword;
import edu.yuhf.domain.User;
import edu.yuhf.domain.UserDetail;
import edu.yuhf.service.ArticleTypeServiceImpl;
import edu.yuhf.service.HobbyServiceImpl;
import edu.yuhf.service.NativePlaceServiceImpl;
import edu.yuhf.service.UserServiceImpl;
import edu.yuhf.service.iface.ArticleTypeService;
import edu.yuhf.service.iface.HobbyService;
import edu.yuhf.service.iface.NativePlaceService;
import edu.yuhf.service.iface.UserService;


@WebServlet("/user.servlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static Logger log=Logger.getLogger(UserServlet.class);
	
	private UserService userService=new UserServiceImpl();
	private HobbyService hobbyService=new HobbyServiceImpl();
	private NativePlaceService nativePlaceService=new NativePlaceServiceImpl();
	private ArticleTypeService articleTypeService=new ArticleTypeServiceImpl();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out=response.getWriter();
		
		String param=request.getParameter("param");
		
		String id=request.getParameter("id");
		String ids=request.getParameter("ids");
		String name=null==request.getParameter("userName")?"":request.getParameter("userName");
		String password=request.getParameter("password");
		String sex=null==request.getParameter("sex")?"":request.getParameter("sex");
		String email=request.getParameter("email");
		String cityCode=request.getParameter("cityCode");
		String hobby=null==request.getParameter("hobby")?"":request.getParameter("hobby");
		String[] favs=request.getParameterValues("fav");
		
		String currentPage=null==request.getParameter("currentPage")?"1":request.getParameter("currentPage");
		String isCookie=null==request.getParameter("isCookie")?"no":request.getParameter("isCookie");
		
		if(Objects.equals("init", param)) {
			Page<List<Map<String,Object>>> page=new Page<>(Integer.valueOf(currentPage));
			List<Hobby> hobbyList=hobbyService.getHobbies();
			UMQueryKeyword qk=null;
			if(Objects.equals("", name) && Objects.equals("", sex) && Objects.equals("", hobby)) {
				userService.showByPage(page);
				userService.getTotalRow(page);
			}else {
				qk=new UMQueryKeyword(name,sex,hobby);
				userService.showByPage(page,qk);
				userService.getTotalRow(page,qk);	
			}
			page.setTotalPage(0==page.getTotalRow()%page.getRowNumber()?page.getTotalRow()/page.getRowNumber():page.getTotalRow()/page.getRowNumber()+1);
			hobbyService.setHobbyName(page.getPageData());
			request.setAttribute("users",page.getPageData());
			request.setAttribute("page", page);
			request.setAttribute("hobbies", hobbyList);
			if(null!=qk) {
				request.setAttribute("querykey", qk);
			}
			request.getRequestDispatcher("views/userManager.jsp").forward(request, response);
		}
		if(Objects.equals("queryId", param)) {
			Map<String,Object> map=userService.showUserById(ids);
			List<Hobby> hobbyList=hobbyService.getHobbiesSelected(String.valueOf(map.get("HOBBY_CODE")));
			List<NativePlace> provinceList=nativePlaceService.setProvinceSelected(String.valueOf(map.get("NATIVEPLACE_CODE")));
			List<NativePlace> cityList=nativePlaceService.setCitySelected(String.valueOf(map.get("NATIVEPLACE_CODE")));
			request.setAttribute("user", map);
			request.setAttribute("hobbies", hobbyList);
			request.setAttribute("provinces", provinceList);
			request.setAttribute("cities", cityList);
			request.setAttribute("edit", "on");
			request.getRequestDispatcher("views/userManager.jsp").forward(request, response);
			
		}
		if(Objects.equals("doUpdate", param)) {
			int userId=Integer.valueOf(id);
			User user=new User(userId,name,password,sex,email);
			UserDetail ud=new UserDetail(0,cityCode,String.join(",", favs),userId);
			if(userService.updateUser(user, ud)) {
				out.print("<script>alert('修改成功！');window.location.href='user.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('修改失败！');window.location.href='user.servlet?param=init'</script>");
			}
		}		
		if(Objects.equals("delete", param)) {
			
			if(userService.removeUsers(ids)) {
				out.print("<script>alert('删除成功！');window.location.href='user.servlet?param=init'</script>");
			}else {
				out.print("<script>alert('删除失败！');window.location.href='user.servlet?param=init'</script>");
			}
		}
		if(Objects.equals("doRegister", param)) {
			User user=new User(0,name,password,sex,email);
			UserDetail ud=new UserDetail(0,cityCode,String.join(",", favs),0);
			if(userService.register(user, ud)) {
				out.print("<script>alert('注册成功，请在登录页面登录。');window.location.href='init.servlet'</script>");
			}else {
				out.print("<script>alert('注册失败，请换个时间再试一次。');window.location.href='init.servlet'</script>");
			}
		}
		if(Objects.equals("register", param)) {
			List<Hobby> hobbyList=hobbyService.getHobbies();
			List<NativePlace> provinceList=nativePlaceService.getProvince();
			request.setAttribute("hobbies", hobbyList);
			request.setAttribute("provinces", provinceList);
			request.getRequestDispatcher("register.jsp").forward(request, response);
		}
		if(Objects.equals("login",param)) {
			if(userService.login(name, password)) {
				//cookie操作
				if(Objects.equals(isCookie, "yes")) {
					Cookie cookie=new Cookie("web","www.nnblog.com");
					cookie.setMaxAge(60*60*24);
					response.addCookie(cookie);
				}
				//用户状态校验 Map<name,session>
				ServletContext servletContext=request.getServletContext();
				Map<String,HttpSession> sessionRecord=(Map<String,HttpSession>)servletContext.getAttribute("record");
				if(null==sessionRecord) {
					sessionRecord=new ConcurrentHashMap<String,HttpSession>();
					servletContext.setAttribute("record",sessionRecord);
				}
				if(sessionRecord.containsKey(name)) {
					sessionRecord.get(name).invalidate();	//踢人
				}
				HttpSession session=request.getSession();
				session.setAttribute("name", name);
				sessionRecord.put(name, session);
				request.setAttribute("parentTypes", articleTypeService.getAllType());
				request.getRequestDispatcher("views/main.jsp").forward(request, response);
			}else {
				request.setAttribute("msg","用户名或密码错误!!!");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
		}
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
