package edu.yuhf.web.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@WebListener
public class SessionStatusListener implements HttpSessionListener {

    public SessionStatusListener() {
    }


    public void sessionCreated(HttpSessionEvent se)  { 
    }


    public void sessionDestroyed(HttpSessionEvent se)  { 
    	HttpSession session = se.getSession();
    	System.out.println(session.getAttribute("name")+",有人使用你的帐号登录，你被踢下线了。");
    	//session.getServletContext().getRequestDispatcher("").forward(request, response);
    }
	
}
