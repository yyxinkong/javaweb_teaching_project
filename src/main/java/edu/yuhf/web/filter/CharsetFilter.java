package edu.yuhf.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

import org.apache.log4j.Logger;


@WebFilter(urlPatterns= {"/*"},initParams= {@WebInitParam(name="charset",value="utf-8")})
public class CharsetFilter implements Filter {

	private static Logger log=Logger.getLogger(CharsetFilter.class);
	private String charsetValue;
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(charsetValue);
		response.setCharacterEncoding(charsetValue);
		response.setContentType("text/html; charset="+charsetValue);
		chain.doFilter(request, response);
	}


	public void init(FilterConfig fConfig) throws ServletException {
		charsetValue=fConfig.getInitParameter("charset");
	}


	@Override
	public void destroy() {
		
	}

}
