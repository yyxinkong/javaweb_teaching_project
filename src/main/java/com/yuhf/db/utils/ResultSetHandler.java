package com.yuhf.db.utils;

import java.sql.ResultSet;

public interface ResultSetHandler<T> {

	public T handler(ResultSet rs);
}
