package com.yuhf.db.utils;

import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class JdbcTemplate {

	private static Logger log=Logger.getLogger(JdbcTemplate.class);
	
	public static <T> T query(String sql,ResultSetHandler<T> rsh,Object[] param) {
		T t=null;
		try {
			PreparedStatement psmt=DBConnection.getConnection().prepareStatement(sql);
			ParameterMetaData pmd=psmt.getParameterMetaData();
			if(param!=null&&param.length!=0&&param.length==pmd.getParameterCount()) {
				for(int i = 0,len=param.length;i<len;i++) {
					psmt.setObject(i+1, param[i]);
				}
			}
			ResultSet rs=psmt.executeQuery();
			t=rsh.handler(rs);
		} catch (SQLException e) {
			log.error("query method error,the exception is:"+e.getMessage());
		} finally {
			try {
				if(DBConnection.getConnection().getAutoCommit()) {
					DBConnection.closeConnection(DBConnection.getConnection());
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return t;
	}
	
	public static int  updateForKey(String sql,String[] cols,Object...param) {
		int userId=0;
		int rowNumber=0;
		try {
			PreparedStatement psmt=DBConnection.getConnection().prepareStatement(sql,cols);
			ParameterMetaData pmd=psmt.getParameterMetaData();
			log.debug(param.length+","+pmd.getParameterCount());
			if(param!=null&&param.length!=0&param.length+1==pmd.getParameterCount()) {
				for(int i = 0,len=param.length;i<len;i++) {
					psmt.setObject(i+1, param[i]);
				}				
			}
			rowNumber=psmt.executeUpdate();
			if(rowNumber!=0) {
				ResultSet rs=psmt.getGeneratedKeys();
				if(rs.next()){
					userId=Integer.valueOf(rs.getString(1));
				}
			}
		} catch (SQLException e) {
			log.error("updateforkey method error,this exception is:"+e.getMessage());
		} finally {
			try {
				if(DBConnection.getConnection().getAutoCommit()) {
					DBConnection.closeConnection(DBConnection.getConnection());
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return userId;		
	}
	
	public static int update(String sql,Object...param) {
		int rowNumber=0;
		try {
			PreparedStatement psmt=DBConnection.getConnection().prepareStatement(sql);
			ParameterMetaData pmd=psmt.getParameterMetaData();
			if(param!=null&&param.length!=0&param.length==pmd.getParameterCount()) {
				for(int i = 0,len=param.length;i<len;i++) {
					psmt.setObject(i+1, param[i]);
				}				
			}
			DBUtils.printRealSql(sql, param);
			rowNumber=psmt.executeUpdate();
		} catch (SQLException e) {
			log.error("update method error,this exception is:"+e.getMessage());
		} finally {
			try {
				if(DBConnection.getConnection().getAutoCommit()) {
					DBConnection.closeConnection(DBConnection.getConnection());
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowNumber;		
	}
	
	public static int queryForCount(String sql,Object...param) {
		int rowNumber=0;
		try {
			PreparedStatement psmt=DBConnection.getConnection().prepareStatement(sql);
			ParameterMetaData pmd=psmt.getParameterMetaData();
			if(param!=null&&param.length!=0&param.length==pmd.getParameterCount()) {
				for(int i = 0,len=param.length;i<len;i++) {
					psmt.setObject(i+1, param[i]);
				}				
			}
			DBUtils.printRealSql(sql, param);
			ResultSet rs=psmt.executeQuery();
			if(rs.next()) {
				rowNumber=rs.getInt(1);
			}
		} catch (SQLException e) {
			log.error("queryForCount method error,this exception is:"+e.getMessage());
		} finally {
			try {
				if(DBConnection.getConnection().getAutoCommit()) {
					DBConnection.closeConnection(DBConnection.getConnection());
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowNumber;
	}
}
