<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>主页面</title>
<link rel="stylesheet" href="../css/base.css">
<link rel="stylesheet" href="../css/main.css">

</head>
<body>
	<main>
	<header>logo</header>
	<nav>
		<h2 style="text-align: center">管理菜单</h2>
		<div style="text-align: center">
			<a href="user.servlet?param=init" target="mainPage">用户信息管理</a><br>
			<a href="#" target="mainPage">写文章</a><br>
			<a href="#" target="mainPage">文章类型</a>
		</div>
		<h2 style="text-align: center">文章类型</h2>
		<ul id="leftMenu">
			<c:forEach items="${requestScope.parentTypes}" var="articleType">
				<li class="leftMenuLi1">${articleType.name}<div class="leftMenuLi1_image"></div></li>
				<ul id="leftSubMenu">
				<c:forEach items="${articleType.subArticleTypes}" var="subArticleType">
					<li class="leftMenuLi2">
						<a class="navigate" target="mainPage" href="articleType.servlet?typeId=${subArticleType.id}">${subArticleType.name}</a>
					</li>
				</c:forEach>
				</ul>
			</c:forEach>
		</ul>
	</nav>
	<article>
		<section>
			<iframe name="mainPage" src="views/welcome.jsp"></iframe>
		</section>
	</article>
	</main>
</body>
</html>