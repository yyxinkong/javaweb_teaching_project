<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>文章分类页面</title>
<link rel="stylesheet" href="../css/base.css">
<style>
#document_content table{
	border-collapse:separate; border-spacing:0px 20px;
}
#document_content{
	margin-top:10px;
}
#document_content dd{
	margin-left: 10px;
}
#document_content dt{
	overflow: hidden;
}
#document_content dl{
	margin:0px;
}
.document_title{float:left;font-weight: 700;font: bold 16px/25px '微软雅黑';}
.document_title_date{
	margin-left:50px;
	font:normal 12px/20px '微软雅黑';
}
.document_praise{
	width:80px;
	height: 20px;
	margin-top:10px;
	margin-left:10px;
}
.document_praise:hover{
	cursor:pointer;
}
.document_add_praise{
	background:url(../img/ooopic_1506740300.png) no-repeat 0px 0px/20px 20px;
}
.document_minus_praise{
	background:url(../img/ooopic_1506740309.png) no-repeat 0px 0px/20px 20px;
}
.document_praise_text{
	margin-left:25px;
	font-weight: bold;
}
.docuemnt_type{float:right}
td a:link,td a:hover,td a:active,td a:visited{
	text-decoration: none;
	color:black;
	cursor:pointer;
}
tbody,tfoot{
	border:1px solid #ccc;
}
.articleTypeRow{
	border:1px solid #ccc;
}
#pageNavigate{
	border:1px solid #ccc;
}
</style>
</head>
<body>
<nav id="pageNavigate">文章信息管理->文章类型显示</nav>
<section id="main_content">
	<nav id="document_content">
	<table style="width:100%;">
	<tbody>
	<c:forEach items="${requestScope.articles}" var="map">
		<tr class="articleTypeRow">
			<td style="width:100%">
				<a href="">
				<dl>
					<dt>
						<div class="document_title">${map.get('TITLE')}<span class="document_title_date">更新时间：${map.get('PUBLISHTIME')}</span></div>
						<div class="docuemnt_type">
						${map.get('PARENTNAME')}
						->
						${map.get('SUBNAME')}
						</div>
					</dt>
					<dd><pre><c:out value="${map.get('CONTENT')}"></c:out></pre></dd>
				</dl>
				</a>
				<div style="float:left" class="document_praise document_add_praise">
					<span class="document_praise_text">(${map.get('LIKENUMBER')}个赞)</span>
				</div>
				<div style="float:right">文章点击次数(${map.get('CLICK')}次)</div>
			</td>
		</tr>
	</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td>上一页 下一页</td>
		</tr>
	</tfoot>
</table>
</nav>
</section>




</body>
</html>