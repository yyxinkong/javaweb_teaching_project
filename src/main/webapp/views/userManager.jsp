<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>用户信息管理</title>
<link rel="stylesheet" href="../css/base.css">
<link rel="stylesheet" href="../css/userManager.css">
<script src="../js/ajaxUtil.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/userManager.js" type="text/javascript" charset="utf-8"></script>

</head>
<body>
	<nav id="pageNavigate">用户管理模块->用户信息管理页面</nav>
	<article id="userTable">
		<aside id="searchPart">
			<form id="queryForm" action="user.servlet?param=init" method="post">
			<input type="hidden" name="currentPage" id="queryCurrentPage" value="${requestScope.page.currentPage}">
			<span>
				用户名：<input type="search" name=userName placeholder="输入姓名" value="${requestScope.querykey.name}">
				<span style="margin-left:50px;">性别：</span>
				<select id="selSex" name="sex">
					<option value="">性别</option>
					<c:if test="${empty requestScope.querykey.sex}">
						<option value="1">男</option>
						<option value="2">女</option>					
					</c:if>
					<c:if test="${requestScope.querykey.sex==1}">
						<option value="1" selected>男</option>
						<option value="2">女</option>					
					</c:if>
					<c:if test="${requestScope.querykey.sex==2}">
						<option value="1">男</option>
						<option value="2" selected>女</option>					
					</c:if>
				</select>
				<span style="margin-left:50px;">爱好：</span>
				<select id="selHobby" name="hobby">
					<option value="">爱好</option>
					<c:forEach items="${requestScope.hobbies}" var="hobby">
						<c:if test="${empty requestScope.querykey.hobbyCode or requestScope.querykey.hobbyCode!=hobby.code}">
							<option value="${hobby.code}">${hobby.name }</option>
						</c:if>
						<c:if test="${requestScope.querykey.hobbyCode==hobby.code}">
							<option value="${hobby.code}" selected>${hobby.name }</option>
						</c:if>
					</c:forEach>
				</select>
			</span>
			<div style="float:right">
				<button id="btnQuery" type="submit">查询</button>
			</div>
			</form>
		</aside>
		<table class="realTable">
			<thead>
				<tr>
					<th><input type="checkbox" name="allChecked" id="allChecked"
						value="${map.get('ID')}"></th>
					<th>姓名</th>
					<th>密码</th>
					<th>性别</th>
					<th>电子邮箱</th>
					<th>行政区划</th>
					<th>爱好</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${requestScope.users}" var="map">
					<tr style="text-align: center;height:30px">
						<td><input type="checkbox" name="checkUser"
							id="user_${map.get('ID')}" value="${map.get('ID')}"></td>
						<td>${map.get('NAME')}</td>
						<td>${map.get('PASSWORD')}</td>
						<td><c:if test="${map.get('SEX') eq 1}">男</c:if> <c:if
								test="${map.get('SEX') eq 2}">女</c:if></td>
						<td>${map.get('EMAIL')}</td>
						<td><c:if test="${empty map.get('NATIVEPLACE_NAME')}">
								<span style="color: red">未知</span>
							</c:if> <c:if test="${not empty map.get('NATIVEPLACE_NAME')}">
								${map.get('NATIVEPLACE_NAME')}
							</c:if></td>
						<td>${map.get('HOBBY_CODE')}</td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="7">
						<div style="float:left;height:35px;margin-top:10px;">
							<button style="margin-left:10px;" id="btnUpdate">修改</button>
							<button id="btnDelete">删除</button>
						</div>
						<div style="float:right;height:35px;margin-top:10px;">
							当前第${requestScope.page.currentPage}页
							每页${requestScope.page.rowNumber}行，共${requestScope.page.totalRow}行，共<span id="spanTotalPage">${requestScope.page.totalPage}</span>页
							&nbsp;&nbsp;
							<c:if test="${1==requestScope.page.currentPage}">首页&nbsp;&nbsp;上一页</c:if>
							<c:if test="${1!=requestScope.page.currentPage}">
								<a class="btn_page" href="#">首页</a>&nbsp;&nbsp;<a class="btn_page" href="#">上一页</a>
							</c:if>&nbsp;&nbsp;
							<select id="jumpPage">
								<c:forEach begin="1" end="${requestScope.page.totalPage}" step="1" var="i">
									<c:if test="${i==requestScope.page.currentPage}">
										<option value="${i}" selected>${i}</option>
									</c:if>
									<c:if test="${i!=requestScope.page.currentPage}">
										<option value="${i}">${i}</option>
									</c:if>
								</c:forEach>
							</select>
							&nbsp;&nbsp;
							<c:if test="${requestScope.page.totalPage==requestScope.page.currentPage}">下一页&nbsp;&nbsp;尾页</c:if>
							<c:if test="${requestScope.page.totalPage!=requestScope.page.currentPage}">
								<a class="btn_page" href="#">下一页</a>&nbsp;&nbsp;<a class="btn_page" href="#"  style="margin-right:10px;">尾页</a>
							</c:if>
						</div>
					</td>
				</tr>
			</tfoot>
		</table>
	</article>
	<article id="userEdit" hidden>
		<form action="user.servlet?param=doUpdate" method="post">
			<fieldset id="userRegister">
				<legend hidden>用户修改</legend>
				<input type="hidden" name="id" value="${requestScope.user.get('ID')}">
				<table class="registerTable">
					<tbody id="registerTbody">
						<tr>
							<td><label for="userName">用户名：</label></td>
							<td><input type="text" name="userName" id="userName"
								value="${requestScope.user.get('NAME') }" required
								 /></td>
							<td>
								<div id="userNameImage"></div>
								<div id="userNameMessage"></div>
							</td>
						</tr>
						<tr>
							<td><label for="password">密码：</label></td>
							<td><input type="password" name="password" id="password"
								value="${requestScope.user.get('PASSWORD') }" /></td>
							<td>
								<div id="passwordImage"></div>
								<div id="passwordMessage"></div>
							</td>
						</tr>
						<tr>
							<td><label for="rePassword">重复密码：</label></td>
							<td><input type="password" name="rePassword" id="rePassword"
								value="${requestScope.user.get('PASSWORD') }" /></td>
							<td>
								<div id="rePasswordImage"></div>
								<div id="rePasswordMessage"></div>
							</td>
						</tr>
						<tr>
							<td>性别：</td>
							<td colspan="2"><c:if
									test="${requestScope.user.get('SEX') eq 0}">
									<input type="radio" name="sex" id="man" value="1" />
									<label for="man">男</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="sex" id="woman" value="0" checked />
									<label for="woman">女</label>
								</c:if> <c:if test="${requestScope.user.get('SEX') eq 1}">
									<input type="radio" name="sex" id="man" value="1" checked />
									<label for="man">男</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="sex" id="woman" value="0" />
									<label for="woman">女</label>
								</c:if></td>
						</tr>
						<tr>
							<td>爱好：</td>
							<td colspan="2">
								<table>
									<tr>
										<c:forEach items="${requestScope.hobbies}" var="hobby" varStatus="status">
											<td><c:if test="${hobby.selected}">
													<input type="checkbox" name="fav" id="${hobby.code}" value="${hobby.code}" checked />
												</c:if>
												<c:if test="${not hobby.selected}">
													<input type="checkbox" name="fav" id="${hobby.code}" value="${hobby.code}" />
												</c:if>
												<label for="${hobby.code}">${hobby.name}</label>
											<td><c:if test="${(status.index+1)%4==0}"></tr><tr></c:if>
										</c:forEach>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>来自于：</td>
							<td colspan="2"><select id="selProvince">
									<c:forEach items="${requestScope.provinces}" var="province">
										<c:if test="${province.selected}">
											<option value="${province.code}" selected>${province.name}</option>
										</c:if>
										<c:if test="${not province.selected}">
										<option value="${province.code}">${province.name}</option>
										</c:if>
									</c:forEach>
							</select>&nbsp;&nbsp;&nbsp;&nbsp; 
							<select id="selCity" name="cityCode">
									<c:forEach items="${requestScope.cities}" var="city">
										<c:if test="${city.selected}">
											<option value="${city.code}" selected>${city.name}</option>
										</c:if>
										<c:if test="${not city.selected}">
										<option value="${city.code}">${city.name}</option>
										</c:if>
									</c:forEach>
							</select>
							</td>
						</tr>
						<tr>
							<td>email</td>
							<td><input type="text" name="email" id="email"
								value="${requestScope.user.get('EMAIL') }"></td>
							<td>
								<div id="emailImage"></div>
								<div id="emailMessage"></div>
							</td>
						</tr>
					</tbody>
				</table>
				<nav id="btnOperation">
					<button type="reset" class="btn">重置</button>
					&nbsp;&nbsp;
					<button type="submit" class="btn">修改</button>
				</nav>
			</fieldset>
		</form>
	</article>
	<input type="hidden" id="isEdit" value="${requestScope.edit}">
</body>
</html>