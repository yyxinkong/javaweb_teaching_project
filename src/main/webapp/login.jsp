<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录</title>
<link rel="stylesheet" href="css/base.css">
<style>
input[type='text'],input[type='password']{
	margin-top:5px;
	height:20px;
	width:150px;
}
button{
	margin-top:20px;
}
</style>
</head>
<body>

<form action="user.servlet?param=login" method="post">
	用户名：<input type="text" name="userName"><br>
	密&nbsp;&nbsp;&nbsp;码：<input type="password" name="password"><br>
	<input type="checkbox" value="yes" name="isCookie">一天之内不再登录<br>
	<button type="submit">登录</button><button type="submit" formaction="user.servlet?param=register">注册</button>
</form>
<span style="color:red">${requestScope.msg}</span>
</body>
</html>