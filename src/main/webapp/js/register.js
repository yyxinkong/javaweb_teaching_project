function initCity(){
	let ajax=new AjaxUtil();
	let url="nativePlace.servlet?code="+document.getElementById("selProvince").value;
	ajax.get(url,(xhr)=>{
		let json=JSON.parse(xhr.responseText);
		let node=document.getElementById("selCity");
		node.innerHTML="";
		if(json.msg!="no date"){
			let defaultOption=document.createElement("option");
			defaultOption.value="0000";
			defaultOption.innerHTML="请选择城市";
			defaultOption.selected=true;
			node.appendChild(defaultOption);
			json.result.forEach((item)=>{
				let option=document.createElement("option");
				option.value=item.code;
				option.innerHTML=item.name;
				node.appendChild(option);
			});
		}		
	});
	/*
	let xhr=new XMLHttpRequest();
	let url="nativePlace.servlet?code="+document.getElementById("selProvince").value;
	xhr.open("GET",url,true);
	xhr.addEventListener("readystatechange",()=>{
		if(xhr.readyState==4 && xhr.status==200){
			let json=JSON.parse(xhr.responseText);
			let node=document.getElementById("selCity");
			node.innerHTML="";
			if(json.msg!="no date"){
				let defaultOption=document.createElement("option");
				defaultOption.value="0000";
				defaultOption.innerHTML="请选择城市";
				defaultOption.selected=true;
				node.appendChild(defaultOption);
				json.result.forEach((item)=>{
					let option=document.createElement("option");
					option.value=item.code;
					option.innerHTML=item.name;
					node.appendChild(option);
				});
			}
		}
	},false);
	xhr.send(null);
	*/
}
function init(){
	document.getElementById("selProvince").addEventListener("change",initCity,false);
}
window.addEventListener("load",init,false);