class AjaxUtil{
	constructor(){}
	get(url,callback){
		let xhr=new XMLHttpRequest();
		xhr.open("GET",url,true);
		xhr.addEventListener("readystatechange",()=>{
			if(xhr.readyState==4 && xhr.status==200){
				callback(xhr);
			}
		});
		xhr.send(null);
	}
}