function initCity(){
	let ajax=new AjaxUtil();
	let url="nativePlace.servlet?code="+document.getElementById("selProvince").value;
	ajax.get(url,(xhr)=>{
		let json=JSON.parse(xhr.responseText);
		let node=document.getElementById("selCity");
		node.innerHTML="";
		if(json.msg!="no date"){
			let defaultOption=document.createElement("option");
			defaultOption.value="0000";
			defaultOption.innerHTML="请选择城市";
			defaultOption.selected=true;
			node.appendChild(defaultOption);
			json.result.forEach((item)=>{
				let option=document.createElement("option");
				option.value=item.code;
				option.innerHTML=item.name;
				node.appendChild(option);
			});
		}		
	});
}
function deleteOperation(){
	let nodes=document.getElementsByName("checkUser");
	let params="";
	Array.from(nodes).filter(item=>item.checked).forEach((item)=>{
		if(""==params){
			params+=item.value;
		}else{
			params+=",";
			params+=item.value;
		}
	});
	if(params==""){
		alert("没有相应的数据，请重新选择数据！");
	}else{
		window.location.href="user.servlet?param=delete&ids="+params;
	}
}
function updateOperation(){
	let nodes=document.getElementsByName("checkUser");
	let params="";
	let count=0;
	Array.from(nodes).filter(item=>item.checked).forEach((item)=>{
		if(""==params){
			params+=item.value;
		}else{
			params+=",";
			params+=item.value;
		}
		count++;
	});
	if(params==""){
		alert("没有相应的数据，请重新选择数据！");
	}else if(count>1){
		alert("一次只能修改一个数据");
	}else{
		window.location.href="user.servlet?param=queryId&ids="+params;
	}
}
function pagination(event){
	event.preventDefault();
	let nextPage=0;
	let currentPage=document.getElementById("queryCurrentPage").value;
	if(event.target.className=="btn_page"){
		switch(event.target.innerHTML){
			case "首页":nextPage=1;break;
			case "下一页":nextPage=Number.parseInt(currentPage)+1;break;
			case "上一页":nextPage=Number.parseInt(currentPage)-1;break;
			case "尾页":nextPage=document.getElementById("spanTotalPage").innerHTML;break;
		}
	}else if(event.target.id=="jumpPage"){
		nextPage=document.getElementById("jumpPage").value;
	}
	document.getElementById("queryCurrentPage").value=nextPage;
	document.getElementById("queryForm").submit();
	//window.location.href="user.servlet?param=init&currentPage="+nextPage;
}
function init(){
	let btns=document.querySelectorAll(".btn_page");
	Array.from(btns).forEach((item)=>{
		item.addEventListener("click",pagination,false)  ;
	});
	document.getElementById("jumpPage").addEventListener("change",pagination,false);
	document.getElementById("selProvince").addEventListener("change",initCity,false);
	if('on'==document.getElementById("isEdit").value){
		document.getElementById("userEdit").hidden=false;
		document.getElementById("userTable").hidden=true;
	}
	document.getElementById("allChecked").addEventListener("click",()=>{
		let nodes=document.getElementsByName("checkUser");
		let flag=false;
		if(document.getElementById("allChecked").checked){
			flag=true;
		}
		nodes.forEach((item)=>{
			item.checked=flag;
		});
	},false);
	document.getElementById("btnDelete").addEventListener("click",deleteOperation,false);
	document.getElementById("btnUpdate").addEventListener("click",updateOperation,false);
}
window.addEventListener("load",init,false);
